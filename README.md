# PyROOTTutorial

This is a template of what I was thinking for in terms of the Friday morning tutorial for the November FIRST-HEP HSF event.  I am not an official "ROOT educator" but have some experience working through previous tutorials like this and also have an understanding of what Mason Profitt intends to cover on Friday afternoon.  

## GenData.py

This notebook generates the toy MC that is then inspected/analyzed in the AnalyzeData notebook.  This is not intended for students or to be presented but rather makes this a self-contained thing.

## AnalyzeData.py

This notebook is intended to be the content-focus of the morning.  It can be recasted to a Swan notebook if one of the learning goals is to demonstrate EOS functionality to participants and how to integrate this with Jupyter (Swan) notebooks. This consists of a few main parts : 
- How to access/inspect TFiles 
- How to access content in a TTree
- Writing an event loop to "analyze" data and go from TTree to TH1F (and storing in another TFile)
- Normalizing signal and background to data - covers additional functionality (TH1F::Scale) 
- (Advanced) - If students blow through the first part, they can then be tasked with writing a template fit from scratch - covers the functionality of how to access the specific bin contents in a histogram (TH1F::GetBinContent)